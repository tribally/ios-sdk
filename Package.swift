// swift-tools-version:5.5.0
import PackageDescription

let package = Package(
    name: "Tribally",
    platforms: [
        .iOS(.v13),
    ],
    products: [
        .library(
            name: "Tribally",
            targets: ["Tribally"]
        ),
    ],
    dependencies: [],
    targets: [
        .binaryTarget(
            name: "Tribally",
            url: "https://gitlab.com/tribally/ios-sdk/uploads/951f41b83de7e278732f6aa0345ab83a/Tribally.zip",
            checksum: "04a8d912fb3b824cac1f95a69d8dd19ad8130dc66b36705c7cd9843c16c93cfa"
        ),
    ]
)
