## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have installed the latest version of Xcode. (Above Xcode 12 Recommended)
- Tribally works for the iOS devices from iOS 13 and above.

---

## Install Tribally

## 1. Setup

To use SDK, you need to first register on Tribally Dashboard. [Click here to sign up](https://admin.tribally.app/signin).

### i. Get your Application Keys

- Create a new project
- Head over to the Settings section and note the `Project ID`, `API Key`

---

### ii. Add Tribally Dependency

**SPM:** 

Go to Project → Package Dependencies → Press “+” and type

[https://gitlab.com/tribally/ios-sdk](https://gitlab.com/tribally/ios-sdk) into search


**CocoaPods:** 

`pod 'Tribally'`



---

## Configure TriballyViewController inside your app

You can initialize TriballyViewController with TriballyConfiguration, for example:

```swift
import Tribally

TriballyViewController(
   configuration: .init(
      projectId: "Tribally Project ID",
      apiKey: "Tribally API Key",
      externalId: "User ID on your platform",
      displayName: "Display name",
      avatar: .init(string: "Avatar image URL(optional)")
   )
)
```

## **Notes**

- Make sure you replace the `projectId` with your Tribally `projectId` and `apiKey` with your `apiKey` in the above code.
- You need to call `TriballyAuth.logOut()` after switching to another `projectId` .
- As Tribally has support for adding photos, please add `Camera Usage Description` and `Photo Library Usage Description` to your `.plist` file.
