
Pod::Spec.new do |s|
    s.name         = "Tribally"
    s.version      = "0.4.4"
    s.summary      = "Tribally supercharges any app with social features"
    s.description  = "Tribally SDKs enable your users to create communities and bring in more people to talk about the things they love."
    s.homepage     = "http://tribally.app/"
    s.authors       = { 'Tribally ' => 'support@tribally.app' }
    s.license = { :type => 'Copyright', :text => "Copyright Tribally Inc." }
    s.source       = { :git => "https://gitlab.com/tribally/ios-sdk", :tag => "#{s.version}" }
    s.vendored_frameworks = "Tribally.xcframework"
    s.platform = :ios
    s.swift_version = "5.0"
    s.ios.deployment_target  = '13.0'
end
